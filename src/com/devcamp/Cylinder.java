package com.devcamp;

public class Cylinder extends Circle {
    double height = 1.0;

    public Cylinder(){
    }

    public Cylinder(double radius){
        this.radius = radius;
    }

    public Cylinder(double radius, String color){
        this.radius = radius;
        this.color = color;
    }

    public Cylinder(double radius, double height, String color){
        this.radius = radius;
        this.height = height;
        this.color = color;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getVolume(){
        return Math.PI * radius * radius * height;
    }

    @Override
    public String toString() {
        return "Cylinder [radius= " + radius +  ", height= " + height + ", color= " + color + "]";
    }
}
